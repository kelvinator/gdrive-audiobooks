import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { BooksPage } from '../pages/books/books';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PlaybackPage } from '../pages/playback/playback';
import { PlaybackPageModule } from '../pages/playback/playback.module';
import { CurrentTimeProvider } from '../providers/current-time/current-time';
import { BooksPageModule } from '../pages/books/books.module';
import { AudioFilesPage } from '../pages/audio-files/audio-files';
import { AudioFilesPageModule } from '../pages/audio-files/audio-files.module';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SignInPageModule } from '../pages/sign-in/sign-in.module';
import { AuthProvider } from '../providers/auth/auth';
import { GdriveProvider } from '../providers/gdrive/gdrive';

import {prodEnvironment as environment} from '../firebase-config';
import { CacheProvider } from '../providers/cache/cache';
import { DurationProvider } from '../providers/duration/duration';
import { ListenedToProvider } from '../providers/listened-to/listened-to';
import { SettingsProvider } from '../providers/settings/settings';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    BooksPageModule,
    AudioFilesPageModule,
    PlaybackPageModule,
    SignInPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PlaybackPage,
    BooksPage,
    AudioFilesPage,
    SignInPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CurrentTimeProvider,
    AuthProvider,
    GdriveProvider,
    CacheProvider,
    DurationProvider,
    ListenedToProvider,
    SettingsProvider
  ]
})
export class AppModule {}

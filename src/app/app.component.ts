import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';

import { AuthProvider } from '../providers/auth/auth';
import {SignInPage} from '../pages/sign-in/sign-in';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'books';

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, private authProvider: AuthProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Books', component: 'books' },
      { title: 'Settings', component: 'settings' },
      { title: 'Sign Out', component: null }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.authProvider.userSubscription.subscribe((user) => {
        if(!user) {
          this.nav.setRoot('SignInPage');
        } else {
          // if(window.location.hash === '#/sign-in') {
          //   this.nav.setRoot('books');
          // }
          const currentView = this.nav.getActive();
          if (currentView && currentView.instance instanceof SignInPage ){
            this.nav.setRoot('books');
          }
        }
      });

      // if(!this.authProvider.user) {
      //   firstPage = {
      //     pageName: page,
      //     pageArgs: pageArgs
      //   };
      // } else {
      //   firstPage = null;
      //   this.nav.push(page, pageArgs);
      // }
    });
  }

  openPage(page) {
    if(page.title === 'Sign Out') {
      this.authProvider.signOut();
      return;
    }
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

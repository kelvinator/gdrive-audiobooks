import { Injectable } from '@angular/core';
import { set, get, Store } from 'idb-keyval';

@Injectable()
export class SettingsProvider {
  settingsStore: Store;

  constructor() {
    // need new db name because of bug in idb-keyval https://github.com/jakearchibald/idb-keyval/issues/31
    this.settingsStore = new Store('gdriveaudio-settings', 'settings');
  }

  saveSettings(settingName: string, val: any): Promise<void> {
    return set(settingName, val, this.settingsStore);
  }

  getSettings(settingName: string): Promise<any> {
    return get(settingName, this.settingsStore);
  }
}

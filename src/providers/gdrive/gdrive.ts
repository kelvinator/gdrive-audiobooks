import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {from} from 'rxjs/observable/from'
import {concatMap, filter} from 'rxjs/operators';
declare let gapi: any;

@Injectable()
export class GdriveProvider {
 
  CLIENT_ID: string = '783417483076-h623afutlte3ajop1hd3t3fcav196r33.apps.googleusercontent.com';
  API_KEY: string = 'AIzaSyDHZrCiQby9XpBtNoIeJuf4RJc8Qlps25U';

  // Array of API discovery doc URLs for APIs used by the quickstart
  DISCOVERY_DOCS: string[] = ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest'];

  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  SCOPES: string = 'https://www.googleapis.com/auth/drive.readonly';

  gapiLoaded: boolean = false;

  gapiLoadedSubject = new BehaviorSubject(this.gapiLoaded);

  constructor() {
    gapi.load('client', () => {
      gapi.client.init({
        apiKey: this.API_KEY,
        clientId: this.CLIENT_ID,
        discoveryDocs: this.DISCOVERY_DOCS,
        scope: this.SCOPES
      }).then(() => {
        // console.log('Done', wat);
        // console.log('Is signed in', gapi.auth2.getAuthInstance().isSignedIn.get());
        this.gapiLoaded = true;
        this.gapiLoadedSubject.next(this.gapiLoaded);
      });
    });
  }

  getBooks(rootFolderID: string): Observable<any> {
    const listArgs = {
      'pageSize': 100,
      'fields': "nextPageToken, files(id, name)",
      'q': `'${rootFolderID}' in parents and mimeType = 'application/vnd.google-apps.folder' and trashed = false`,
      'orderBy': 'name'
    }
    
    return this.gapiLoadedSubject.pipe(
      filter(loaded => {return loaded}),
      concatMap(() => {
        return from(gapi.client.drive.files.list(listArgs));    
      })
    );
  }

  getAudioFiles(bookId: string): Observable<any> {
    const listArgs = {
      'pageSize': 100,
      'fields': "nextPageToken, files(id, name, description, mimeType)",
      'q': `'${bookId}' in parents and trashed = false and (mimeType = 'audio/mp3' or mimeType = 'audio/mpeg' or mimeType = 'audio/ogg')`,
      'orderBy': 'name'
    }

    return this.gapiLoadedSubject.pipe(
      filter(loaded => {return loaded}),
      concatMap(() => {
        return from(gapi.client.drive.files.list(listArgs));    
      })
    );
  }

}

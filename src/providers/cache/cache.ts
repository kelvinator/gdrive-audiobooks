import { Injectable } from '@angular/core';

@Injectable()
export class CacheProvider {

  constructor() {
  }

  hasFileInCache(reqURL: string): Promise<any[]> {
    return caches.open('gdrive-audio-files').then(c => {
      return c.keys();
    }).then(keys => {
      return keys.filter(k => {
        return k.url === reqURL;
      });
    });
  }

  addToCache(reqURL: string): Promise<any> {
    const req = new Request(reqURL);
    return fetch(req).then((res: Response) => {
      return caches.open('gdrive-audio-files').then((c: Cache) => {
        return c.put(req, res);
      });
    });
  }

  removeFromCache(reqURL: string): Promise<any> {
    const req = new Request(reqURL);
    return caches.open('gdrive-audio-files').then(c => {
      return c.delete(req).then(() => {
          console.log('done deleting from cache');
          return;
      });
    });
  }

}

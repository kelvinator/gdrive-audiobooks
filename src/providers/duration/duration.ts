import { Injectable } from '@angular/core';
import { set, get, Store } from 'idb-keyval';

@Injectable()
export class DurationProvider {
  durationStore: Store;

  constructor() {
    // need new db name because of bug in idb-keyval https://github.com/jakearchibald/idb-keyval/issues/31
    this.durationStore = new Store('gdriveaudio-duration', 'duration');
  }

  saveDuration(fileName: string, val: number) {
    return set(fileName, val, this.durationStore);
  }

  getDuration(fileName: string) {
    return get(fileName, this.durationStore);
  }
}

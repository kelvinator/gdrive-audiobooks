import { Injectable } from '@angular/core';
import { DurationProvider } from '../../providers/duration/duration';
import { CurrentTimeProvider } from '../../providers/current-time/current-time';

@Injectable()
export class ListenedToProvider {

  recalcThis: string = null;

  constructor(private durationProvider: DurationProvider, private currentTimeProvider: CurrentTimeProvider) {
  }

  calculatePercentage(audioFileName: string): Promise<any> {
    let duration;
    return this.durationProvider.getDuration(audioFileName).then(result => {
      if(result) {
        duration = result;
        return this.currentTimeProvider.getCurrentTime(audioFileName);
      }
      return Promise.resolve(null);
    }).then((currentTime) => {
      let percent = 0;
      if(currentTime !== null) {
        percent = Math.round(currentTime/duration*100);
      }
      return {name: audioFileName, listenedTo: `${percent}%`};
    }).catch(() => {
      return {name: audioFileName, listenedTo: '0%'};
    });
  }

}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {map, take} from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
declare let gapi: any;

@Injectable()
export class AuthProvider {

  public userSubscription: Observable<any>;
  
  public signingOut$: Subject<void> = new Subject<void>();

  public user: firebase.User = null;

  constructor(public afAuth: AngularFireAuth) {
    this.userSubscription = this.subscribeToUser();
  }

  subscribeToUser(): Observable<firebase.User> {
    return this.afAuth.authState.pipe(
      map(user => {
        this.user = user;
        return user;
      })
    );
  }

  canActivate(): Promise<void> {
    if(this.user) return Promise.resolve();
    return this.subscribeToUser().pipe(take(1)).toPromise().then((user: firebase.User): Promise<void> => {
      if(user !== null) {
        return Promise.resolve()
      } else {
        return Promise.reject(null);
      }      
    });
  }

  getJWT() {
    return this.user.getIdToken(/* forceRefresh */ true);
  }

  prepareAccessToken(): Promise<any> {
    return new Promise((resolve, reject) => {
      const authResponse = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse(true);
      const tokenExpiresAt = new Date(authResponse.expires_at);
      if(new Date() > tokenExpiresAt) {
        console.log('Need new token');
        gapi.auth2.getAuthInstance().currentUser.get().reloadAuthResponse().then(response => {
          resolve(response.access_token);
        }).catch(reject);
      } else {
        resolve(authResponse.access_token);
      }
    }).then(accessToken => {
      // send it to the service worker
      if(navigator.serviceWorker.controller) {
        navigator.serviceWorker.controller.postMessage(accessToken);
        return Promise.resolve();
      } else {
        console.log('no serviceworker controller');
        return Promise.reject('No serviceworker controller. Reload page?');
      }
    });   
  }

  signIn(): Promise<any> {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/drive.readonly');
    return this.afAuth.auth.signInWithRedirect(provider).catch((error: any) => {
      // Handle Errors here.
      alert(error.message);
    });
  }

  signOut() {
    this.signingOut$.next();
    this.afAuth.auth.signOut();
  }


}
import { Injectable } from '@angular/core';
import { set, get, Store } from 'idb-keyval';
/*
  Generated class for the CurrentTimeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CurrentTimeProvider {
  
  currentTimeStore: Store;

  constructor() {
    this.currentTimeStore = new Store('gdriveaudio-db', 'currentTime');
  }

  saveCurrentTime(fileName: string, val: number) {
    return set(fileName, val, this.currentTimeStore);
  }

  getCurrentTime(fileName: string) {
    return get(fileName, this.currentTimeStore);
  }

}

let bearerToken;

self.addEventListener('install', (event) => {
  event.waitUntil(self.skipWaiting()); // Activate worker immediately
});

self.addEventListener('activate', (event) => {
  event.waitUntil(self.clients.claim()); // Become available to all pages
});

self.addEventListener('fetch', (event) => {
  const requestURL = new URL(event.request.url);
  if(requestURL.hostname === 'www.googleapis.com' &&  (requestURL.pathname && requestURL.pathname.substring(0, 15) === '/drive/v3/files')) {
    event.respondWith(
      fetchInterceptResponder(event)
    )
  }
});

self.addEventListener('message', (event) =>{
  bearerToken = event.data;
});

function sendMessageToClient(clientId, msgObj) {
  self.clients.get(clientId).then(c => {
    c.postMessage(msgObj);
  });
}

function fetchInterceptResponder(event) {
  return self.caches.open('gdrive-audio-files').then(c => {
      return c.match(event.request);
  }).then(res => {
      if(res) {
          return respondAudioFromCache(event.request, res);
      } else {
          return fetchAudioFromNetwork(event.request);
      }                
  }).catch(error => {
    sendMessageToClient(event.clientId, error);
  });
}

function respondAudioFromCache(eventRequest, cachedResponse) {
  const rangeHeaderValue = eventRequest.headers.get('range');
  const byteStart = rangeHeaderValue.substring(6, rangeHeaderValue.indexOf('-'));;
  
  return cachedResponse.blob().then(blob => {
      const blobSize = blob.size;
      let fakeHeaders = new Headers();
      // fakeHeaders.set('cache-control', 'max-age=-1');
      fakeHeaders.set('accept-ranges', 'bytes');
      // fakeHeaders.set('connection', 'keep-alive');
      fakeHeaders.set('content-length', blobSize -  byteStart);
      fakeHeaders.set('content-range', `bytes ${byteStart}-${blobSize - 1}/${blobSize}`);
      fakeHeaders.set('content-type', 'audio/mpeg');
      fakeHeaders.set('server', 'service-worker');
      return new Response(blob.slice(byteStart), {
          status: 206,
          statusText: 'Partial Content',
          headers: fakeHeaders
      });
  });   
}

function fetchAudioFromNetwork(eventRequest) {
  let newHeaders = new Headers();
  for (const [header, value] of eventRequest.headers) {
      newHeaders.append(header, value);
  }
  newHeaders.append('Authorization', `Bearer ${bearerToken}`);

  return fetch(eventRequest.url, {
      method: eventRequest.method,
      headers: newHeaders,
      redirect: eventRequest.redirect
  }).then(response => {
      if(!response.ok) {
        return response.json().then(err => {
          let error = {message: `Network response was not ok.`, code: response.status};
          if(err.error  && err.error.message) {
            error.message = err.error.message;
          }
          return Promise.reject(error)
        });
        
      }
      
      return response;
  });
}
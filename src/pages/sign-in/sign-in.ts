import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  loader: Loading = null;

  constructor(public navCtrl: NavController,  private loadingCtrl: LoadingController, private authProvider: AuthProvider) {
  }

  signInGoogle() {
    this.showLoader();
    this.authProvider.signIn();
  }

  showLoader() {
    this.loader = this.loadingCtrl.create({
      content: "Signing In...",
      dismissOnPageChange: true
    });
    this.loader.present();
  }

}

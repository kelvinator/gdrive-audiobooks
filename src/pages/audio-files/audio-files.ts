import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { GdriveProvider } from '../../providers/gdrive/gdrive';
import {take} from 'rxjs/operators';
import { ListenedToProvider } from '../../providers/listened-to/listened-to';

@IonicPage({
  name: 'audioFiles',
  segment: 'audioFiles/:bookTitle/:bookId',
  defaultHistory: ['books']
})
@Component({
  selector: 'page-audio-files',
  templateUrl: 'audio-files.html',
})
export class AudioFilesPage implements OnInit {

  audioFiles: Array<{name: string, src: string, id: string, listenedTo: string}>;

  bookTitle: string;

  bookId: string;

  fakeFiles: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  constructor(public navCtrl: NavController, public navParams: NavParams, private authProvider: AuthProvider,
      private gdriveProvider: GdriveProvider, private listenedToProvider: ListenedToProvider) {
    this.bookTitle = this.navParams.get('bookTitle');
    this.bookId = this.navParams.get('bookId');
  }

  ngOnInit() {
    this.getData(null);    
  }

  getData(refresher) {
    this.gdriveProvider.getAudioFiles(this.bookId).pipe(take(1)).subscribe((response: any) => {
      const files = response.result.files;
      if(files) {
        Promise.all(files.map(f => {
          return this.listenedToProvider.calculatePercentage(f.name);
        })).then((listenedToArray: any) => {
          this.audioFiles = files.map(f => {
            return {
              name: f.name,
              src: `https://www.googleapis.com/drive/v3/files/${f.id}?alt=media`,
              id: f.id,
              listenedTo: listenedToArray.find(l => {return l.name === f.name}).listenedTo
            }
          });
          if(refresher) {
            refresher.complete();
          }
        });
      }
    });
  }

  ionViewCanEnter() {
    return this.authProvider.canActivate().catch(() => {
      this.navCtrl.setRoot('SignInPage');
    });
  }

  ionViewDidEnter() {
    if(this.listenedToProvider.recalcThis) {
      const recalcTarget = this.listenedToProvider.recalcThis;
      this.listenedToProvider.calculatePercentage(recalcTarget).then((result: any) => {
        this.audioFiles.find(af => {return af.name === result.name}).listenedTo =  result.listenedTo      
        this.listenedToProvider.recalcThis = null;
      });
    }  
  }

  fileTapped(ev, f) {
    this.navCtrl.push('PlaybackPage', {
      audioSrc: f.src,
      audioName: f.name
    })
  }

}

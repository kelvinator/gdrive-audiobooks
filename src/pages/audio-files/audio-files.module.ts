import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AudioFilesPage } from './audio-files';

@NgModule({
  declarations: [
    AudioFilesPage,
  ],
  imports: [
    IonicPageModule.forChild(AudioFilesPage),
  ],
  entryComponents: [
    AudioFilesPage
  ]
})
export class AudioFilesPageModule {}

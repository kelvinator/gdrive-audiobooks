import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { GdriveProvider } from '../../providers/gdrive/gdrive';
import {take} from 'rxjs/operators';
import { SettingsProvider } from '../../providers/settings/settings';

@IonicPage({
  name: 'books',
  segment: 'books'
})
@Component({
  selector: 'page-books',
  templateUrl: 'books.html',
})
export class BooksPage implements OnInit {
  books: Array<{title: string, author: string, id: string}> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private authProvider: AuthProvider,
    private gdriveProvider: GdriveProvider, private settingsProvider: SettingsProvider) {
  }

  ngOnInit() {
    this.getData(null);
  }

  getData(refresher) {
    // this.books = [];
    this.settingsProvider.getSettings('rootFolder').then(rootFolderID => {
      // get google drive books
      this.gdriveProvider.getBooks(rootFolderID).pipe(take(1)).subscribe((response: any) => {
        const files = response.result.files;
        if(files) {
          this.books = files.map((f: any) => {
            return {
              title: f.name,
              author: '',
              id: f.id
            };
          });
        }
        if(refresher) {
          refresher.complete();
        }
      });
    });
    
  }

  ionViewCanEnter() {
    return this.authProvider.canActivate().catch(() => {
      this.navCtrl.setRoot('SignInPage');
    });
  }

  bookTapped(ev, book) {
    this.navCtrl.push('audioFiles', {
      bookTitle: book.title,
      bookId: book.id
    });
  }

}

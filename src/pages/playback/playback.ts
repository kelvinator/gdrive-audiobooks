import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { ListenedToProvider } from '../../providers/listened-to/listened-to';

@IonicPage({
  name: 'PlaybackPage',
  segment: 'playback/:audioName/:audioSrc'
})
@Component({
  selector: 'page-playback',
  templateUrl: 'playback.html',
})
export class PlaybackPage implements OnInit, OnDestroy {

  audioFile: any;

  accessTokenReady: boolean = false;

  boundVisibilityHandler: EventListenerObject;

  constructor(public navCtrl: NavController, public navParams: NavParams, private authProvider: AuthProvider,
    private toastCtrl: ToastController, private listenedToProvider: ListenedToProvider) {
  }

  ngOnInit() {
    this.audioFile = {
      src: this.navParams.get('audioSrc'),
      name: this.navParams.get('audioName')
    };
    this.prepareToken();

    navigator.serviceWorker.addEventListener('message', (event) =>{
      const toast = this.toastCtrl.create({
        message: event.data.message,
        duration: 3000,
        position: 'bottom'
      });
    
      toast.present();

      if(event.data.code === 401) {
        // if the service worker is all of a sudden getting unauthorized cause it slept send it the access token again.
        this.prepareToken();
      }
    });

    this.boundVisibilityHandler = this.visibilityChangeHandler.bind(this);
    document.addEventListener('visibilitychange', this.boundVisibilityHandler);
  }

  visibilityChangeHandler() {
    if(!document.hidden) {
      this.prepareToken();
    }
  }

  prepareToken() {
    this.authProvider.prepareAccessToken().then(() => {
      this.accessTokenReady = true;
    }).catch((errorMessage: string) => {
      const toast = this.toastCtrl.create({
        message: errorMessage,
        closeButtonText: 'Ok',
        showCloseButton: true,
        position: 'bottom'
      });
    
      toast.onDidDismiss(() => {
        window.location.reload(true);
      });
      toast.present();
    });
  }

  ionViewDidEnter() {
    this.listenedToProvider.recalcThis = this.audioFile.name;
  }

  ionViewCanEnter() {
    return this.authProvider.canActivate().catch(() => {
      this.navCtrl.setRoot('SignInPage');
    });
  }

  ngOnDestroy() {
    document.removeEventListener('visibilitychange', this.boundVisibilityHandler);
  }

}

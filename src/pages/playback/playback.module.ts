import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaybackPage } from './playback';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PlaybackPage,
  ],
  imports: [
    IonicPageModule.forChild(PlaybackPage),
    ComponentsModule
  ],
  entryComponents: [
    PlaybackPage
  ]
})
export class PlaybackPageModule {}

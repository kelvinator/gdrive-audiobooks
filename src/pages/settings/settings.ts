import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController } from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SettingsProvider } from '../../providers/settings/settings';

@IonicPage({
  name: 'settings',
  segment: 'settings'
})
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {

  avatarSrc: string;

  userDisplayName: string;

  loader: Loading;

  settingsForm: FormGroup = new FormGroup ({
    rootFolder: new FormControl('', [Validators.required])
  });

  private submitting: boolean = false;

  constructor(private navCtrl: NavController, private authProvider: AuthProvider, private settingsProvider: SettingsProvider,
      private loadingCtrl: LoadingController) {
  }

  ngOnInit() {
    this.avatarSrc = this.authProvider.user.photoURL;
    this.userDisplayName = this.authProvider.user.displayName;
    // get current root folder id
    this.settingsProvider.getSettings('rootFolder').then(val => {
      if(val) {
        this.settingsForm.setValue({rootFolder: val});
      }
    }); 
  }

  saveSettings() {
    if(!this.settingsForm.valid || this.submitting) return;
    this.submitting = true;
    this.toggleLoader();
    const rootFolder = this.settingsForm.value.rootFolder;
    this.settingsProvider.saveSettings('rootFolder', rootFolder).then(() => {
      this.toggleLoader();
      this.submitting = false;
    });
  }

  toggleLoader() {
    if(this.loader) {
      this.loader.dismiss();
      this.loader = null;
    } else {
      this.loader = this.loadingCtrl.create({
        content: 'Saving...',
        dismissOnPageChange: true
      });
      this.loader.present();
    }    
  }

  ionViewCanEnter() {
    return this.authProvider.canActivate().catch(() => {
      this.navCtrl.setRoot('SignInPage');
    });
  }

}

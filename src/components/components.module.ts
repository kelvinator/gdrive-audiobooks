import { NgModule } from '@angular/core';
import { AudioPlayerComponent } from './audio-player/audio-player';
import { IonicModule } from 'ionic-angular';
import { PlaybackToolbarComponent } from './playback-toolbar/playback-toolbar';
@NgModule({
	declarations: [AudioPlayerComponent,
    PlaybackToolbarComponent],
	imports: [IonicModule],
	exports: [AudioPlayerComponent,
    PlaybackToolbarComponent]
})
export class ComponentsModule {}

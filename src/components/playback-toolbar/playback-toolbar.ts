import { Component, OnInit, Input } from '@angular/core';
import { CacheProvider } from '../../providers/cache/cache';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'playback-toolbar',
  templateUrl: 'playback-toolbar.html'
})
export class PlaybackToolbarComponent implements OnInit {
  @Input() audioFile: any;

  isFileCached: boolean;

  constructor(private cacheProvider: CacheProvider, private toastCtrl: ToastController) {
  }

  ngOnInit() {
    this.cacheProvider.hasFileInCache(this.audioFile.src).then(result => {
      this.isFileCached = result.length > 0;
    })
  }

  toggleCache() {
    let toastMessage;
    if(!this.isFileCached) {
      this.cacheProvider.addToCache(this.audioFile.src).then(() => {
        this.isFileCached = !this.isFileCached;
      }).catch(() => {
        // have a catch but do nothing so the toast for fetch error still shows up.
      });
      toastMessage = `Caching ${this.audioFile.name}`;
    } else {
      this.cacheProvider.removeFromCache(this.audioFile.src).then(() => {
        this.isFileCached = !this.isFileCached;
      });
      toastMessage = `Removing ${this.audioFile.name} from cache`;
    }

    this.showToast(toastMessage);
    
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 2500,
      position: 'bottom'
    });
  
    toast.present();
  }
}

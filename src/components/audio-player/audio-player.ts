import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { CurrentTimeProvider } from '../../providers/current-time/current-time';
import { LoadingController, Loading } from 'ionic-angular';
import { DurationProvider } from '../../providers/duration/duration';

interface AudioFile {
  name: string,
  src: string
}

@Component({
  selector: 'audio-player',
  templateUrl: 'audio-player.html'
})
export class AudioPlayerComponent implements OnInit, OnDestroy {

  @Input() audioFile: AudioFile;

  audioStream: HTMLAudioElement;

  paused: boolean;

  currentTime: number;

  duration: number;

  loader: Loading = null;

  firstLoad: boolean = true;

  constructor(private currentTimeProvider: CurrentTimeProvider, private loadingCtrl: LoadingController,
    private durationProvider: DurationProvider) {
  }

  ngOnInit() {
    this.showLoader();

    this.audioStream = new Audio();

    this.audioStream.addEventListener('timeupdate', () => {
      if(this.audioStream === null) return;
      // display the current time
      this.currentTime = this.audioStream.currentTime;
    });

    this.audioStream.addEventListener('durationchange', () => {
      this.duration = this.audioStream.duration;
      // save the duration
      this.durationProvider.saveDuration(this.audioFile.name, this.duration);
    });

    this.audioStream.addEventListener('canplaythrough', () => {
      if(this.audioStream.readyState > 3) {
        if(this.firstLoad) {
          // get the saved current time if any
          this.currentTimeProvider.getCurrentTime(this.audioFile.name).then((result: number) => {
            if(result) {        
              this.audioStream.currentTime = result;
            }
            this.firstLoad = false;
            if(!result) {
              this.hideLoader();
            }            
          });
        } else {
          this.hideLoader();
        }
        
      }      
    });

    this.audioStream.addEventListener('seeking', () => {
      this.showLoader();
    });

    this.audioStream.addEventListener('ended', () => {
      this.currentTimeProvider.saveCurrentTime(this.audioFile.name, this.audioStream.currentTime - 1);
    });

    this.audioStream.src = this.audioFile.src;

    window.addEventListener('beforeunload', this._beforeUnloadHandler.bind(this), false);
  }

  showLoader() {
    if(this.loader) return;
    this.loader = this.loadingCtrl.create({
      content: 'Reticulating Splines',
      dismissOnPageChange: false
    });
    this.loader.present();
  }

  hideLoader() {
    if(!this.loader) return;
    this.loader.dismiss();
    this.loader = null;
  }

  seek(amount: number) {
    if(this.audioStream.paused) {
      this.audioStream.currentTime = this.audioStream.currentTime + amount;
    } else {
      this.audioStream.pause();
      this.audioStream.currentTime = this.audioStream.currentTime + amount;
      this.audioStream.play();
    }
  }


  togglePlayback() {
    this.paused = this.audioStream.paused;

    if(this.audioStream.paused) {
      this.audioStream.play();
    } else {
      this.audioStream.pause();
      // save current time
      this.currentTimeProvider.saveCurrentTime(this.audioFile.name, this.audioStream.currentTime);
    }
  }

  _beforeUnloadHandler() {
    this.currentTimeProvider.saveCurrentTime(this.audioFile.name, this.audioStream.currentTime);
  }

  ngOnDestroy() {
    if(!this.audioStream.paused) {
      this.audioStream.pause();
    }
    this.currentTimeProvider.saveCurrentTime(this.audioFile.name, this.audioStream.currentTime);
  }

}

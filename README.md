# GDrive Audiobooks

### What is this?
This is a PWA to stream audiobooks from your Google Drive. You can eventually check it out [here](https://gdrive-audiobooks.firebaseapp.com). Eventually because right now if you sign in you will only see a blank books page. This is because I haven't added code to handle users who aren't me. I am currently working on this feature.
### Why are you building this?
I borrow audiobooks from my local library which lets me download them as MP3s. I upload those MP3s to my Google Drive so I can listen to them on my phone. But the Google Drive native app isn't specifically designed for audiobooks. It doesn't save your playback position, it doesn't integrate with Chromecasts, and it doesn't have an in-app dictionary lookup. I also wanted to learn more about the service worker and cache storage by using them in a useful app.
